<?php

namespace HotWire\RedPlates;

use HotWire\RedPlates\TemplateStrategy\ITemplateStrategy;
use HotWire\DependencyInjection\Container;

abstract class AbstractTemplate
{
    /**
     * view name
     * @var string
     */
    protected $view;

    /**
     * parameters to pass in view
     * @var array
     */
    protected $parameters=array();

    /**
     * engine type, whether it's twig or plates
     * @var string
     */
    protected $type;

    /**
     * template behavior
     * @var ITemplateStrategy
     */
    protected $templateBehavior;

    /**
     * get parameters from Container, set the type and create the templating behavior
     */
    public function __construct()
    {
        if ($parameters=Container::getInstance()->get('parameters')) {
            $this->type=$parameters->getTemplating();
        }
        $this->createTemplatingBehavior();
    }

    /**
     * Gets the value of view.
     *
     * @return mixed
     */
    public function getView()
    {
        return $this->view;
    }

    /**
     * Sets the value of view.
     *
     * @param mixed $view the view
     *
     * @return self
     */
    public function setView($view)
    {
        $this->view = $view;

        return $this;
    }

    /**
     * Gets the value of parameters.
     *
     * @return mixed
     */
    public function getParameters()
    {
        return $this->parameters;
    }

    /**
     * Sets the value of parameters.
     *
     * @param mixed $parameters the parameters
     *
     * @return self
     */
    public function setParameters($parameters)
    {
        $this->parameters = $parameters;

        return $this;
    }

    /**
     * Gets the value of type.
     *
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Sets the value of type.
     *
     * @param mixed $type the type
     *
     * @return self
     */
    protected function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Gets the template hehavior
     *
     * @return ITemplateBehavior
     */
    public function getTemplateBehavior()
    {
        return $this->templateBehavior;
    }

    /**
     * Sets the template behavior
     *
     * @param ITemplateBehavior $templateBehavior the template behavior
     *
     * @return self
     */
    public function setTemplateBehavior(ITemplateStrategy $templateBehavior)
    {
        $this->templateBehavior = $templateBehavior;

        return $this;
    }

    /**
     * create templating behavior
     */
    abstract public function createTemplatingBehavior();

    /**
     * render view
     * @return HotWire\Http\Response
     */
    abstract public function render();
}
