<?php

namespace HotWire\RedPlates;

class Template extends AbstractTemplate
{
    /**
     * create templating behavior
     */
    public function createTemplatingBehavior()
    {
        $class=__NAMESPACE__."\\TemplateStrategy\\".ucfirst($this->type).'Strategy';
        if (class_exists($class,true)) {
            $this->templateBehavior=new $class();
        }
    }

    /**
     * render view
     * @return HotWire\Http\Response
     */
    public function render()
    {
        return $this->templateBehavior->render($this->view, $this->parameters);
    }
}
