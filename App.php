<?php

namespace HotWire\RedPlates;

use HotWire\Framework\AbstractApp;

class App extends AbstractApp
{
    /**
     * get module name
     * @return string
     */
    public function getName()
    {
        return 'HotWire:RedPlates';
    }
}
