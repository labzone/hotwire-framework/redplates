<?php

namespace HotWire\RedPlates\TemplateStrategy;

interface ITemplateStrategy
{
    public function initialize();
    public function render($name, $parameters);
}
