<?php

namespace HotWire\RedPlates\TemplateStrategy;

use HotWire\DependencyInjection\Container;

abstract class TemplatingStrategy implements ITemplateStrategy
{
    protected $engine;
    protected $container;

    /**
     * set container and run initialize method
     */
    public function __construct()
    {
        $this->container=Container::getInstance();
        $this->initialize();
    }
}
