<?php

namespace HotWire\RedPlates\TemplateStrategy;

use League\Plates\Engine;

use HotWire\Http\Response;
use HotWire\RedPlates\App;

use HotWire\DependencyInjection\Container;

/**
 * Plates Strategy to deal with plates views
 */
class PlatesStrategy extends TemplatingStrategy
{
    /**
     * initialize templating engine
     */
    public function initialize()
    {
        $this->engine = new Engine(App::getResourcePath());
        if ($kernel=$this->container->get('kernel')) {
            foreach ($kernel->getApps() as $app) {
                if ($path=$app->getViewsPath()) {
                    $this->engine->addFolder($app->getName(), $path);
                }
            }
        }
    }

    /**
     * render view
     * @param  string   $name       view name
     * @param  array    $parameters value to pass in view
     * @return Response
     */
    public function render($name, $parameters = array())
    {
        return new Response($this->engine->render($name, $parameters));
    }
}
