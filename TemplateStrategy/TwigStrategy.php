<?php

namespace HotWire\RedPlates\TemplateStrategy;

use HotWire\RedPlates\App;
use HotWire\Http\Response;

use Twig_Autoloader;
use Twig_Loader_Filesystem;
use Twig_Environment;

/**
 * Twig Strategy to deal with Twig views
 */
class TwigStrategy extends TemplatingStrategy
{
    /**
     * initialize templating engine
     */
    public function initialize()
    {
        Twig_Autoloader::register();
        $loader = new Twig_Loader_Filesystem(App::getResourcePath());
        if ($kernel=$this->container->get('kernel')) {
            foreach ($kernel->getApps() as $app) {
                if ($path=$app->getViewsPath()) {
                    $loader->addPath($path, $app->getName());
                }
            }
        }
        $this->engine = new Twig_Environment($loader, array(
            'cache' => App::getCachePath(),
            'debug' => $this->container->get('parameters')->isDebug()
        ));
    }

    /**
     * render view
     * @param  string   $name       view name
     * @param  array    $parameters value to pass in view
     * @return Response
     */
    public function render($name, $parameters=array())
    {
        $name=str_replace('::', '/', $name);

        return new Response($this->engine->render("@{$name}.html.twig", $parameters));
    }
}
