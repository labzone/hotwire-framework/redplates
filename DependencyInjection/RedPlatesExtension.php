<?php

namespace HotWire\RedPlates\DependencyInjection;

use HotWire\DependencyInjection\Extension;
use HotWire\RedPlates\Template;
use HotWire\RedPlates\TemplateStrategy\TwigStrategy;

class RedPlatesExtension extends Extension
{
    public function load()
    {
        $twig=new Template();
        $twig->setTemplateBehavior(new TwigStrategy());
        $this->container->register('templating', new Template())
                        ->register('templating.twig', $twig);

        return $this;
    }
}
